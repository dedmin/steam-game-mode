#!/bin/bash

######################################################################################
# This is the script for perfomance mode
# if steam is running, screen refresh rate set to 144Hz, radeon profile set auto (perfomance)
# if steam is stops, screen refresh rate set to 60Hz, radeon profile set low 
#
# create configuration file in ~/.config/autostart/ directory and add game_mode.sh to auto-start
#
# Author: https://gitlab.com/dedmin 
# Email dmrepo@proton.me
# Version: 2022-02-06
# License: GPLv3
######################################################################################

set -ueo pipefail

POSITION="1920x1080+1920+0"
MONITOR2="$(xrandr | grep " connected $POSITION" | awk '{print $1}')"
STATE=1
FREQUENCY_UP=144
FREQUENCY_DOWN=60
POS="1920x0"

mode() {
  EXIT_CODE=$(ps -C steam > /dev/null 2>&1; echo $?) || true
  RADEON_SCRIPT=$(radeon-script > /dev/null 2>&1; echo $?) || true
  if [[ $EXIT_CODE -eq 0 ]]; then
    if [[ $STATE -eq 0 ]]; then
      xrandr -r "$FREQUENCY_UP"
      if [[ $RADEON_SCRIPT -eq 1 ]]; then
        sudo radeon-script set auto
      fi
      STATE=1
    fi
  else
    if [[ $STATE -eq 1 ]]; then
      xrandr -r "$FREQUENCY_DOWN"
      xrandr --output "$MONITOR2" --pos "$POS" --auto
      if [[ $RADEON_SCRIPT -eq 1 ]]; then
        sudo radeon-script set low
      fi
      STATE=0
    fi
  fi
}

while true; do
  sleep 2
  mode
done
