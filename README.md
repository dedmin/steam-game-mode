# Scripts

Scripts to manage AMD radeon card and screen frame rate


# Configure

Run it in command line:
```bash
git clone https://gitlab.com/dedmin/steam-game-mode.git && \
cd steam-game-mode && \
sudo sh ./configure.sh
```

Create configuration file in ~/.config/autostart/ directory and add game_mode.sh to auto-start


